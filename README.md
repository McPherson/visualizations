# IDEA - Integrated Dashboard for Energy transition Analysis 
_An open-access integrated visualization dashboard for the energy system models_


## Overview

The repository contains a visualization toolkit to report results from five energy system models developed by Sustainable Energy Systems Group - [SESIT](https://sesit.cive.uvic.ca/) team, [University of Victoria](https://www.uvic.ca/).

1. MESSAGEix-Canada - Integrated Assessment Model for Canada built on [MESSAGEix](https://docs.messageix.org/en/stable/)
2. COPPER (Canadian Opportunities for Planning and Production of Electricity Resources) - Capacity expansion model
3. [SILVER](https://www.sciencedirect.com/science/article/abs/pii/S0360544217312021) - Electricity system operation model
3. SESIT's transportation modelling tool 
5. SESIT's buildings modelling tool

## Usage

Two standard data templates have been used to report results from the models. The details can be seen in the documentation. IDEA is developed using [Panel](https://panel.holoviz.org/) Python package. 

1. Install requirements:

```
pip install -r requirements.txt
```

2. Run the panel application:

```
iPython Combined_Visualization_Current.py
```

3. Install python package:

```
pip3 install IDEA_SESIT
```

## Contributing
We welcome the community to contribute to the project. For any queries or questions, we prefer to use GitLab issues. 


## License

Copyright 2021, SESIT and the developer team

The platform is licensed under the Apache License, Version 2.0 (the "License");
see [LICENSE](https://gitlab.com/McPherson/visualizations/-/blob/master/LICENSE)
