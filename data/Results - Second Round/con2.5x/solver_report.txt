Terminated optimal and noraml ;)


# ==========================================================
# = Solver Results                                         =
# ==========================================================
# ----------------------------------------------------------
#   Problem Information
# ----------------------------------------------------------
Problem: 
- Name: tmp5kiox13c
  Lower bound: 20159693354.919044
  Upper bound: 20159693354.919044
  Number of objectives: 1
  Number of constraints: 1417173
  Number of variables: 857236
  Number of nonzeros: 27396670
  Sense: minimize
# ----------------------------------------------------------
#   Solver Information
# ----------------------------------------------------------
Solver: 
- Status: ok
  User time: 103192.21
  Termination condition: optimal
  Termination message: MIP - Integer optimal solution\x3a Objective = 2.0159693355e+10
  Statistics: 
    Branch and bound: 
      Number of bounded subproblems: 0
      Number of created subproblems: 0
  Error rc: 0
  Time: 103228.92152452469
# ----------------------------------------------------------
#   Solution Information
# ----------------------------------------------------------
Solution: 
- number of solutions: 0
  number of solutions displayed: 0
