Planning for the target year 2050 considering 2018 as the reference year
modeled ['2030', '2040', '2050'] palnning periods and ran 24 representative days in each period
Carbon price = {'2025': 95, '2030': 170, '2035': 220, '2040': 270, '2045': 320, '2050': 370}
reserve margin = 0.15
pumped hydro retrofit limit = 0.2
down sampling clustering ? False
hierarchical clustering ? True
test run ? False
hydro development on ? True
autrarky on ? False
pump as continous variable ? True
provincial_emission_limit on?  False   0.0 carbon reduction compared to refrence year 2017
national_emission_limit on?  True  {'2025': 100, '2030': 11, '2035': 11, '2040': 0, '2045': 0, '2050': 0}
local gas price on? True
OBPS on? False
SMR and CCS technologies ? False
thermal phase out on? True >>>>> if true these types will be phased out by the specified year {'gasSC': '2040', 'gasCC': '2040', 'coal': '2030', 'diesel': '2040', 'biomass': '2040'}
min installed gas PHP requirement on ? False
is developping new thermal banned ? False >>>>> if true development of these types are banned ['coal', 'gasCC', 'gasSC', 'nuclear', 'diesel', 'biomass']
just small hydro on ? False >>>>> if true, the model just consider development of small hydro projects (under 100 MW)
DSF tranmission on ? False
is  technology evolution on? {'base': True, 'evolving': False}
is  GPS on? True
is  CPO on? True
is  tranmission expansion constrained? CTE extant = True CTE coefficient =>> 0, CTE custom = False
is  non_emitting limit on? False  {'2025': 0, '2030': 0.9, '2035': 0, '2040': 0, '2045': 0, '2050': 0}
:imited new thermal generation expansion? {} 
