# A Post Processing Visualization Tool for the SILVER Electricity System Model

## Overview
This tool takes output data from the SILVER electricity system model and visualizes the data in an interactive way on a locally hosted webapp. The webapp platform is based on Streamlit, and the plotting functions are based on Pandas and Bokeh. Data is provided from SILVER model outputs and spatial data collected externally.

## Installation
The non-standard libraries used in this app are:
- pandas
- bokeh
- streamlit
- pyproj
- networkx

These libraries are all open sources and can be pip installed using current versions to any version of python. 
Note: In March 2021,  updates to Bokeh 2.0.0 may not be fully implemented in Streamlit causing issues. If figures are not plotting refer to any open issues in the Streamlit github and downgrade your versions of Bokheh and Streamlit accordingly. (Issue #1134 and #1226) 

## Usage
To run the streamlit webapp, cd into the directory containing SILVER_Post_Process_app.py and run the following command:

`streamlit run SILVER_Post_Process_app.py`

A locally hosted tab will open in your default browser displaying the webapp, and further instruction will appear in the terminal. Using the PyCharm terminal is ideal for running this command.

Once the webapp is loaded in the browser, open settings in the top right hand corner and select "run on save" and "Show app in wide mode" to get maximum usability out of the app. This will allow any updates to the code in the IDE to be updated on the webapp after saving.

All the functions required for processing the data are in the query_SILVER.py file. Data is sorted by province in the file structure provided.

## Contributing
We welcome the community to contribute to the project. For any queries or questions, we prefer to use GitLab issues.

## Support
For any questions, contact Joel Grieco at the following email address:
joelkgrieco@gmail.com
