from flask import Flask, render_template
from flask.wrappers import Request

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/Total Generation Capacity')
def Total_Generation_Capacity():
    return render_template('ABA_generation.html')

@app.route('/Additional Generation Capacity')
def Additional_Generation_Capacity():
    return render_template('Total_installed.html')

@app.route('/Canada Generation Capacity')
def Canada_Generation_Capacity():
    return render_template('Canada_generation.html')

@app.route('/Canada New Installed Generation Capacity')
def Canada_New_Installed_Generation_Capacity():
    return render_template('New_installed.html')

@app.route('/Canada New Installed Transmission Capacity')
def Canada_New_Installed_Transmission_Capacity():
    return render_template('Transmission_capacity.html')

@app.route('/Wind Capacity Build vs. Capacity Factor')
def Wind_Capacity_Build_vs_Capacity_Factor():
    return render_template('Grid_Cell.html')

@app.route('/Map of Installed VRE Capacity')
def Map_of_Installed_VRE_Capacity():
    return render_template('installed_capacity.html')

#@app.route('/silver/stacked_plot')
#def silver_stacked_plot():
#    return render_template('stacked_plot.html')
#
#@app.route('/silver/total_generation_plot')
#def silver_total_generation_plot():
#    return render_template('total_generation_plot.html')

if __name__ == '__main__':
    app.run(debug=True)
