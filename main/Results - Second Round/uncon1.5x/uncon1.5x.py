from bokeh.models.sources import ColumnDataSource, ColumnarDataSource
from bokeh.plotting import figure, show
from bokeh.io import output_file, save
from bokeh.palettes import Spectral4
from bokeh.models import NumeralTickFormatter, BasicTickFormatter, PrintfTickFormatter
import openpyxl
from openpyxl import load_workbook
import pandas as pd

##========= Import the data from Data_summary ============## 
def import_data_summary():
    data_summary = pd.read_excel('Results_summary.xlsx', sheet_name=1, index_col=0, header=0, usecols=[0]+[i for i in range(1,4)], skiprows=range(11,88))
    data_summary = data_summary.T
#    data_summary['province'] = data_summary.index
    data_summary.index.name = 'Province'
#    data_summary = data_summary.rename(index=lambda x: x[:-4], inplace=False)
#    data_summary = data_summary.groupby(['Province'], sort=False).sum()
    data_summary = data_summary[['hydro', 'nuclear', 'coal', 'gasCC', 'gasSC', 'diesel', 'biomass', 'wind', 'solar']]
    data_summary = data_summary/1000
    print(data_summary)
#    a = data_summary.index.tolist()
#    print(a)
    return data_summary

def plot_carbon_limit_data(data_summary):
#    plot = figure(title="With Carbon Limit", x_axis_label='Year', y_axis_label='Emission (Mt CO2)')
    regions = data_summary.columns.tolist()
    source = ColumnDataSource(data_summary)
    province = source.data['Province'].tolist()
#    for i in province:
#        if i[-1] == '1' or '2':
#            i = i.replace(i[-2:], '')
#            print(i)
    colours = ['blue', 'red', 'brown', 'grey', 'lightgrey', 'orange', 'darkgreen', 'lime', 'yellow']
    plot = figure(title="Total new installed capacity - Scenario Unconstrained Transmission at 1.5x baseline demand", x_range=province, y_range=[0,70], plot_height=550, plot_width=650, toolbar_location=None, tools="")
    plot.vbar_stack(regions, x = 'Province', width=0.5, color=colours, source=source, legend_label=regions)
#    for row, name, color in zip(range(0,4),["$0/tonne", "$100/tonne", "$150/tonne", "$200/tonne"], Spectral4):
#        plot.line(x=data_summary.columns, y=data_summary.iloc[row], line_width=3, legend_label=name, color=color)
#        plot.circle(x=data_summary.columns, y=data_summary.iloc[row], size=10, color=color, legend_label=name)
    plot.legend.label_text_font_size = '5pt'
    plot.legend.location = "top_left"
    plot.legend.click_policy="hide"
    plot.xaxis.axis_label = 'Year'
    plot.yaxis.axis_label = 'New Installed Capacity(GW)'
    plot.yaxis.formatter = NumeralTickFormatter(format='0,0')
    plot.xgrid.visible = False
    plot.ygrid.visible = False
    plot.legend.orientation = "horizontal"
#    show(plot)
    output_file("plots/Stacked_bar_chart.html")
    save(plot)
    return
    


def main():
    data_summary = import_data_summary()
    
    plot_carbon_limit_data(data_summary)

if __name__ == "__main__":
    main()
