import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import plotly.graph_objs as go
from dash.dependencies import Input, Output
import numpy as np
from plotly.subplots import make_subplots
import plotly.express as px

# Prep some fake data for a bar graph
df1 = pd.DataFrame(dict(
    bar_y = ['Bar1', 'Bar2'],
    bar_x = [2,3],
    bar_z = [1,2]
))

# Make bar graph
fig1 = px.bar(df1,
              x="bar_x",
              y='bar_y',
              color='bar_z',
              orientation='h',
)

# Add layout attributes
fig1.update_layout(
    xaxis_title="<b> Bar graph title <b>",
    yaxis_title="<b> Bar x axis <b>",
    legend_title="<b> Bar y axis <b>",
    xaxis = dict(
        showgrid=True,
        ticks="",
        showline = False,
        gridcolor = 'white'
    )
)

# Prep some fake data for a line graph
df2 = pd.DataFrame(dict(
    line_y = [3,2,1, 1,2,3],
    line_x = [1,2,3,1,2,3],
    line_group = ['line1','line1','line1','line2','line2','line2']
))

# Make an ugly line graph
fig2 = px.line(
    df2,
    x= 'line_x',
    y= 'line_y',
    color = 'line_group'
)

# Add a number of layout attributes that are distinct from those above
fig2.update_layout(
    shapes=[dict(
      type= 'line',
      fillcolor = 'black',
      line_width=2,
      yref= 'y', y0= 0, y1= 0,
      xref= 'x', x0= 1, x1= 3,
    )],
    xaxis_title="<b> Line graph title <b>",
    yaxis_title="<b> Line x axis <b>",
    legend_title="<b> Line y axis <b>",
    template='simple_white',
    hoverlabel=dict(bgcolor="white")
)

# app = JupyterDash(__name__)
app = dash.Dash()
figs = ['fig1', 'fig2']

app.layout = html.Div([
    html.Div([
        dcc.Graph(id='plot'),

        html.Div([
            dcc.Dropdown(
                id='variables',
                options=[{'label': i, 'value': i} for i in figs],
                value=figs[0]
            )
        ])
    ])
])

@app.callback(
    Output('plot', 'figure'),
    [Input('variables', 'value')])

def update_graph(fig_name):

    if fig_name == 'fig1':
#         fig=go.Figure(go.Scatter(x=[1,2,3], y = [3,2,1]))
        return fig1


    if fig_name == 'fig2':
#         fig=go.Figure(go.Bar(x=[1,2,3], y = [3,2,1]))
        return fig2
        
# app.run_server(mode='external', debug=True)
app.run_server(debug=True,
           use_reloader=False # Turn off reloader if inside Jupyter
          )  

