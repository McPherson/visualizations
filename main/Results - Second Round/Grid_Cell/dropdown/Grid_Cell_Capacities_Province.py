import pandas as pd
import matplotlib as m
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from matplotlib import colors
import matplotlib.gridspec as gridspec
import numpy as np
import panel as pn
from panel.interact import interact, interactive, fixed, interact_manual
from panel import widgets
from bokeh.plotting import figure, show, output_file
from bokeh.models.widgets import Div
from bokeh.io import output_notebook, show
from bokeh.models import ColumnDataSource, Range1d
from PIL import Image, ImageEnhance
import cv2

#class Grid_Cell:
#    grid_cell = 0
#    capacity = 0
#    x = 0
#    y = 0
#
#    def __init__(self, grid_cell):
#        self.grid_cell = grid_cell
#
##### Global Variables #####
#periods = ["2025", "2030", "2035", "2040", "2045", "2050"]
#provinces_full = {
#            'BC': "British Columbia",
#            'AB': "Alberta",
#            'MB': "Manitoba",
#            'NB': "New Brunswick",
#            'NL': "Newfoundland and Labrador",
#            'NS': "Nova Scotia",
#            'ON': "Ontario",
#            'QB': "Quebec",
#            'SK': "Saskatchewan",
#            'PE': "Prince Edward Island"
#            }
#provinces=["BC", "AB", "SK", "MB"]
#
#def grid_plot(grid_list, range_list_x, range_list_y):
#    cmap = colors.ListedColormap(["#ffffff", "#009000", "#009010", "#009020", "#009030", "#009040", "#009050", "#009050", "#009060", "#009070", "#009080", "#009090", "#0090a0", "#0090b0", "#0090c0", "#0090d0", "#0090e0", "#0090f0"])
#
#    bounds1 = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
#    # bounds2 = [0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000]
#    bounds2 = [0, 250, 500, 750, 1000, 1250, 1500, 1750, 2000, 2250, 2500, 2750, 3000, 3250, 3500]
#
#    norm1 = colors.BoundaryNorm(bounds1, cmap.N)
#    norm2 = colors.BoundaryNorm(bounds2, cmap.N)
#
#    fig, ax = plt.subplots(2, len(provinces), sharex='col', sharey='row', figsize=(16*len(provinces),16))
#
#    max_y = 0
#
#    for province in provinces:
#
#        if max(range_list_y[province]) > max_y:
#            max_y = max(range_list_y[province])
#
#    min_y = max_y
#
#    for province in provinces:
#
#        if min(range_list_y[province]) < min_y:
#            min_y = min(range_list_y[province])
#
#    for province in provinces:
#
#        i = provinces.index(province)
#
#        grid1 = grid_list[province + "_CF"]
#        grid2 = grid_list[province + "_Capacity"]
#
#        ax[1][i].imshow(grid1, cmap=cmap, norm=norm1)
#        ax[0][i].imshow(grid2, cmap=cmap, norm=norm2)
#
#        ax[1][i].grid(which='major', axis='both', linestyle='-', color='k', linewidth=1)
#        ax[1][i].set_title(province + " Capacity Factor", fontsize=20)
#        ax[1][i].set_xticks(np.arange(min(range_list_x[province]), max(range_list_x[province])+0.5, 1));
#        ax[1][i].set_yticks(np.arange(min_y, max_y+0.5, 1));
#        ax[1][i].axis([min(range_list_x[province])-0.5, max(range_list_x[province])+0.5, min_y-0.5, max_y+0.5])
#        ax[1][i].set_xlabel('X', fontsize=15)
#
#        ax[0][i].grid(which='major', axis='both', linestyle='-', color='k', linewidth=1)
#        ax[0][i].set_title(province + " Installed Capacity", fontsize=20)
#        ax[0][i].set_xticks(np.arange(min(range_list_x[province]), max(range_list_x[province])+0.5, 1));
#        ax[0][i].set_yticks(np.arange(min_y, max_y+0.5, 1));
#        ax[0][i].axis([min(range_list_x[province])-0.5, max(range_list_x[province])+0.5, min_y-0.5, max_y+0.5])
#
#    ax[1][0].set_ylabel('Y', fontsize=15)
#    ax[0][0].set_ylabel('Y', fontsize=15)
#
#    fig.tight_layout()
#
#    fig.colorbar(m.cm.ScalarMappable(norm=norm1, cmap=cmap), ax=ax[1])
#    fig.colorbar(m.cm.ScalarMappable(norm=norm2, cmap=cmap), ax=ax[0])
#
#    return fig
##    plt.savefig('GridProvince.png')

def import_data_summary(sce,grow):
#    im = Image.open("con1.5.png")
#    size=(3200,800)
##    print(im.size)
#    im = im.resize(size, Image.ANTIALIAS)
#    im.save("New_image.png", quality=95)
#    im = Image.open("New_image.png")
#    output_file('image.html')
#    img = mpimg.imread('con1.5.png')
#    p = figure(x_range=(0,1), y_range=(0,1))
#    p.image_url(url=['con1.5.png'], x=0, y=0, w=1, h=1, anchor="bottom_left")
#    output_notebook()
#    div_image = Div(text="""<img src="con1.5.png" alt="div_image">""", width=500, height=500)
#    windcf = pd.read_csv('windcf.csv', header=None)
#    wind_capacity = pd.read_csv('capacity_wind.csv', header=None)
#    wind_capacity_recon = pd.read_csv('capacity_wind_recon.csv', header=None)
#    map = pd.read_csv('map_gl_to_ba.csv', header=None)
#    coordinate = pd.read_excel('coordinate.xlsx', header=None)
    if sce == "Existing Transmission":
        if grow == "1.5x load growth":
            im = Image.open("con1.5.png")
        elif grow == "2.5x load growth":
            im = Image.open("con2.5.png")
    elif sce == "Unconstrained Transmission Expansion":
        if grow == "1.5x load growth":
            im = Image.open("uncon1.5.png")
        elif grow == "2.5x load growth":
            im = Image.open("uncon2.5.png")
    size=(2400,600)
    im = im.resize(size, Image.ANTIALIAS)
#    writer = pd.ExcelWriter("GridProvince.xlsx", engine='openpyxl')
#    imgplot = plt.imshow(img)
#    cf_list = {}
#    grid_list = {}
#    range_list_x = {}
#    range_list_y = {}
#
#    hour = 0
#
#    for index_wind,row_wind in windcf.iterrows():
#
#            if int(str(windcf.at[index_wind, 0]).split('.')[0]) > int(hour):
#                hour = str(windcf.at[index_wind, 0]).split('.')[0]
#
#                # if int(hour)%100 == 0:
#                    # print(hour)
#
#            grid_cell = str(windcf.at[index_wind, 0]).split('.')[1]
#
#            if windcf.at[index_wind, 1] > 0:
#                if grid_cell in grid_list.keys():
#                    grid_list[grid_cell] += windcf.at[index_wind, 1]
#                else:
#                    grid_list[grid_cell] = windcf.at[index_wind, 1]
#
#    for province in provinces:
#
#        print(province)
#
#        average_list = {}
#        capacity_list = {}
#        excel_index_list = {}
#        x_list = []
#        y_list = []
#        grid1 = np.zeros(shape=(40,155), dtype=float)
#        grid2 = np.zeros(shape=(40,155), dtype=float)
#
#        for i in map.index:
#
#            if map.iat[i, 1].split('.')[0] == provinces_full[province]:
#                average_list[str(i+1)] = Grid_Cell(i+1)
#
#            capacity_list[str(i+1)] = 0
#
#        for index,row in wind_capacity.loc[wind_capacity[0] == "('2050'"].iterrows():
#
#            capacity_list[wind_capacity.iat[index, 1].split(')')[0].split("'")[1]] += wind_capacity.iat[index, 2] + wind_capacity_recon.iat[index, 2]
#
#        for index,row in coordinate.iterrows():
#
#            if str(coordinate.iat[index, 0]) != "nan" and str(coordinate.iat[index, 0]) != "lon" and provinces_full[province] == coordinate.iat[index, 2]:
#
#                average_list[str(coordinate.iat[index, 5])].x = coordinate.iat[index, 4]
#                average_list[str(coordinate.iat[index, 5])].y = coordinate.iat[index, 3]
#
#                x_list.append(coordinate.iat[index, 4])
#                y_list.append(coordinate.iat[index, 3])
#
#        for i in grid_list:
#            if i in average_list.keys():
#                average_list[i].capacity = grid_list[i]
#
#        for i in average_list:
#            excel_list = {}
#            average_list[i].capacity /= int(hour)
#            excel_list["x"] = average_list[i].x
#            excel_list["y"] = average_list[i].y
#            excel_list["Average Windcf"] = average_list[i].capacity
#            excel_list["Wind Capacity"] = capacity_list[i]
#            excel_index_list[i] = excel_list
#            grid1[average_list[i].y][average_list[i].x] = average_list[i].capacity
#            grid2[average_list[i].y][average_list[i].x] = capacity_list[i]
#
#        grid_list[province + "_CF"] = grid1
#        grid_list[province + "_Capacity"] = grid2
#        range_list_x[province] = x_list
#        range_list_y[province] = y_list
#
#    fig = grid_plot(grid_list, range_list_x, range_list_y)
    return im

def change_plot(Scenario, Growth, view_fn=import_data_summary):
    return view_fn(Scenario, Growth)

def main():
#    data_summary = import_data_summary()
    interact_dict = dict(Scenario=["Existing Transmission", "Unconstrained Transmission Expansion"], Growth=["1.5x load growth", "2.5x load growth"])
    i = pn.interact(change_plot, **interact_dict)
#    plot_carbon_limit_data(data_summary)
#    i.pprint()
    p = pn.Row(i[1],i[0])
    p.save('plots/Grid_Cell.html', embed=True)
    p.show()
if __name__ == "__main__":
    main()
