from bokeh.models.sources import ColumnDataSource, ColumnarDataSource
from bokeh.plotting import figure, show
from bokeh.io import output_file, save
from bokeh.palettes import Spectral4
from bokeh.models import Title, NumeralTickFormatter, BasicTickFormatter, PrintfTickFormatter
import openpyxl
from openpyxl import load_workbook
import pandas as pd
import panel as pn
from panel.interact import interact, interactive, fixed, interact_manual
from panel import widgets

##========= Import the data from Data_summary ============## 
def import_data_summary(sce, grow):
    if sce == "Existing Transmission":
        if grow == "1.5x load growth":
            ran = 80
            sname = "Constrained Transmission at 1.5x"
            data_summary = pd.read_excel('Results_summary_1.5.xlsx', sheet_name=1, index_col=0, header=0, usecols=[0]+[i for i in range(1,4)], skiprows=range(11,88))
            data_summary = data_summary.T

        elif grow == "2.5x load growth":
            ran = 150
            sname = "Constrained Transmission at 2.5x"
            data_summary = pd.read_excel('Results_summary_2.5.xlsx', sheet_name=1, index_col=0, header=0, usecols=[0]+[i for i in range(1,4)], skiprows=range(11,88))
            data_summary = data_summary.T
                    
    elif sce == "Unconstrained Transmission Expansion":
        if grow == "1.5x load growth":
            ran = 70
            sname = "Unconstrained Transmission at 1.5x"
            data_summary = pd.read_excel('Results_summary_un1.5.xlsx', sheet_name=1, index_col=0, header=0, usecols=[0]+[i for i in range(1,4)], skiprows=range(11,88))
            data_summary = data_summary.T
        if grow == "2.5x load growth":
            ran = 150
            sname = "Unconstrained Transmission at 2.5x"
            data_summary = pd.read_excel('Results_summary_un2.5.xlsx', sheet_name=1, index_col=0, header=0, usecols=[0]+[i for i in range(1,4)], skiprows=range(11,88))
            data_summary = data_summary.T

    ran = 150
#    data_summary = data_summary.T
#    data_summary = data_summary.rename(index=lambda x: x[:-4], inplace=False)
    data_summary.index.name = 'Year'
#    data_summary = data_summary.groupby(['Province'], sort=False).sum()
    data_summary = data_summary[['hydro', 'nuclear', 'coal', 'diesel', 'biomass', 'gasCC', 'gasSC', 'wind', 'solar']]
    data_summary = data_summary/1000
    data_summary.rename(columns={"gasCC":"Natural Gas CC", "gasSC":"Natural Gas SC"})
    print(data_summary)
#    a = data_summary.index.tolist()
#    print(a)
#    return data_summary

    regions = data_summary.columns.tolist()
    source = ColumnDataSource(data_summary)
    year = source.data['Year'].tolist()
    colours = ['blue', '#EB2F06', '#CF6A87', '#B71540', '#78E08F', '#95AFC0', '#535C68',  '#079992', '#F6B93B']
    plot = figure(title=f"Canada New Installed Generation Capacity", x_range=year, y_range=[0,ran], plot_height=550, plot_width=750, toolbar_location=None, tools="")
    plot.vbar_stack(regions, x = 'Year', width=0.5, color=colours, source=source, legend_label=['Hydro', 'Nuclear', 'Coal', 'Diesel', 'Biomass', 'Natural Gas CC', 'Natural Gas SC', 'Wind', 'Solar'])
    plot.add_layout(Title(text=f"Scenario: {sname} baseline demand"), 'above')
#    plot.add_layout(Title(text=f"Year: {year}"), 'above')
    plot.legend.label_text_font_size = '10pt'
    plot.add_layout(plot.legend[0], 'right')
    plot.legend[0].items.reverse()
#    plot.legend.location = "top_left"
    plot.legend.click_policy="hide"
    plot.xaxis.axis_label = 'Year'
    plot.yaxis.axis_label = 'New Installed Capacity (GW)'
    plot.yaxis.formatter = NumeralTickFormatter(format='0,0')
    plot.xgrid.visible = False
    plot.ygrid.visible = False
    plot.axis.axis_label_text_font_style = 'bold'
#    plot.legend.orientation = "horizontal"
#    show(plot)
#    output_file("plots/Stacked_bar_chart.html")
#    save(plot)
    return plot

def change_plot(Scenario, Growth, view_fn=import_data_summary):
    return view_fn(Scenario, Growth)
    

def main():
#    data_summary = import_data_summary()
    interact_dict = dict(Scenario=["Existing Transmission", "Unconstrained Transmission Expansion"], Growth=["1.5x load growth", "2.5x load growth"])
    i = pn.interact(change_plot, **interact_dict)
#    plot_carbon_limit_data(data_summary)
#    i.pprint()
    p = pn.Row(i[1],i[0])
    p.save('plots/New_installed.html', embed=True)
    p.show()
if __name__ == "__main__":
    main()
