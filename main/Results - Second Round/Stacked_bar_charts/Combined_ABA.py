from bokeh.models.sources import ColumnDataSource, ColumnarDataSource
from bokeh.plotting import figure, show
from bokeh.io import output_file, save
from bokeh.palettes import Spectral4
from bokeh.models import Title, NumeralTickFormatter, BasicTickFormatter, PrintfTickFormatter
import openpyxl
from openpyxl import load_workbook
import pandas as pd
import panel as pn
from panel.interact import interact, interactive, fixed, interact_manual
from panel import widgets

##========= Import the data from Data_summary ============## 
def import_data_summary(sce, grow, year, reg):
    if sce == "Existing Transmission":
        if grow == "1.5x load growth":
            sname = "Constrained Transmission at 1.5x"
            if reg == "West":
                width = 750
                if year == "2030":
                    ran = 50
                    data_summary = pd.read_excel('Results_summary_1.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(1,5)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-2], inplace=False)
                elif year == "2040":
                    ran = 66
                    data_summary = pd.read_excel('Results_summary_1.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(14,18)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-4], inplace=False)
                elif year == "2050":
                    ran = 66
                    data_summary = pd.read_excel('Results_summary_1.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(27,31)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-4], inplace=False)
            elif reg == "All":
                width = 1500
                if year == "2030":
                    ran = 68
                    data_summary = pd.read_excel('Results_summary_1.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(1,14)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-2], inplace=False)
                elif year == "2040":
                    ran = 68
                    data_summary = pd.read_excel('Results_summary_1.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(14,27)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-4], inplace=False)
                elif year == "2050":
                    ran = 68
                    data_summary = pd.read_excel('Results_summary_1.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(27,40)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-4], inplace=False)

        elif grow == "2.5x load growth":
            sname = "Constrained Transmission at 2.5x"
            if reg == "West":
                width = 750
                if year == "2030":
                    ran = 70
                    data_summary = pd.read_excel('Results_summary_2.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(1,5)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-2], inplace=False)
                elif year == "2040":
                    ran = 110
                    data_summary = pd.read_excel('Results_summary_2.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(14,18)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-4], inplace=False)
                elif year == "2050":
                    ran = 120
                    data_summary = pd.read_excel('Results_summary_2.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(27,31)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-4], inplace=False)
            elif reg == "All":
                width = 1500
                if year == "2030":
                    ran = 80
                    data_summary = pd.read_excel('Results_summary_2.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(1,14)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-2], inplace=False)
                elif year == "2040":
                    ran = 120
                    data_summary = pd.read_excel('Results_summary_2.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(14,27)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-4], inplace=False)
                elif year == "2050":
                    ran = 120
                    data_summary = pd.read_excel('Results_summary_2.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(27,40)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-4], inplace=False)
                    
    elif sce == "Unconstrained Transmission Expansion":
        if grow == "1.5x load growth":
            sname = "Unconstrained Transmission at 1.5x"
            if reg == "West":
                width = 750
                if year == "2030":
                    ran = 30
                    data_summary = pd.read_excel('Results_summary_un1.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(1,5)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-2], inplace=False)
                elif year == "2040":
                    ran = 40
                    data_summary = pd.read_excel('Results_summary_un1.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(14,18)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-4], inplace=False)
                elif year == "2050":
                    ran = 50
                    data_summary = pd.read_excel('Results_summary_un1.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(27,31)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-4], inplace=False)
            elif reg == "All":
                width = 1500
                if year == "2030":
                    ran = 75
                    data_summary = pd.read_excel('Results_summary_un1.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(1,14)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-2], inplace=False)
                elif year == "2040":
                    ran = 75
                    data_summary = pd.read_excel('Results_summary_un1.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(14,27)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-4], inplace=False)
                elif year == "2050":
                    ran = 75
                    data_summary = pd.read_excel('Results_summary_un1.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(27,40)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-4], inplace=False)
        if grow == "2.5x load growth":
            sname = "Unconstrained Transmission at 2.5x"
            if reg == "West":
                width = 750
                if year == "2030":
                    ran = 40
                    data_summary = pd.read_excel('Results_summary_un2.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(1,5)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-2], inplace=False)
                elif year == "2040":
                    ran = 65
                    data_summary = pd.read_excel('Results_summary_un2.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(14,18)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-4], inplace=False)
                elif year == "2050":
                    ran = 85
                    data_summary = pd.read_excel('Results_summary_un2.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(27,31)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-4], inplace=False)
            elif reg == "All":
                width = 1500
                if year == "2030":
                    ran = 90
                    data_summary = pd.read_excel('Results_summary_un2.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(1,14)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-2], inplace=False)
                elif year == "2040":
                    ran = 120
                    data_summary = pd.read_excel('Results_summary_un2.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(14,27)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-4], inplace=False)
                elif year == "2050":
                    ran = 160
                    data_summary = pd.read_excel('Results_summary_un2.5.xlsx', sheet_name=2, index_col=0, header=0, usecols=[0]+[i for i in range(27,40)], skiprows=range(11,88))
                    data_summary = data_summary.T
                    data_summary = data_summary.rename(index=lambda x: x[:-4], inplace=False)

    if reg == "West":
        regname = 'Western Provinces'
    elif reg == "All":
        regname = 'All Provinces'
    ran = 160
#    if year == "2030":
#        start_year = "2018"
#    elif year == "2040":
#        start_year = "2030"
#    elif year == "2050":
#        start_year = "2040"
    
#    data_summary = data_summary.T
#    data_summary = data_summary.rename(index=lambda x: x[:-4], inplace=False)
    data_summary.index.name = 'Province'
    data_summary = data_summary.groupby(['Province'], sort=False).sum()
    data_summary = data_summary[['hydro', 'nuclear', 'coal', 'diesel', 'biomass', 'gasCC', 'gasSC', 'wind', 'solar']]
    data_summary = data_summary/1000
    data_summary.rename(columns={"gasCC":"Natural Gas CC", "gasSC":"Natural Gas SC"})
    print(data_summary)
#    a = data_summary.index.tolist()
#    print(a)
#    return data_summary

#def plot_carbon_limit_data(data_summary):
#    plot = figure(title="With Carbon Limit", x_axis_label='Year', y_axis_label='Emission (Mt CO2)')
    regions = data_summary.columns.tolist()
    source = ColumnDataSource(data_summary)
    province = source.data['Province'].tolist()
#    for i in province:
#        if i[-1] == '1' or '2':
#            i = i.replace(i[-2:], '')
#            print(i)
    colours = ['blue', '#EB2F06', '#CF6A87', '#B71540', '#78E08F', '#95AFC0', '#535C68',  '#079992', '#F6B93B']
    plot = figure(title=f"Total Generation Capacity - {regname}", x_range=province, y_range=[0,ran], plot_height=550, plot_width=width, toolbar_location=None, tools="")
    plot.vbar_stack(regions, x = 'Province', width=0.5, color=colours, source=source, legend_label=['Hydro', 'Nuclear', 'Coal', 'Diesel', 'Biomass', 'Natural Gas CC', 'Natural Gas SC', 'Wind', 'Solar'])
#    for row, name, color in zip(range(0,4),["$0/tonne", "$100/tonne", "$150/tonne", "$200/tonne"], Spectral4):
#        plot.line(x=data_summary.columns, y=data_summary.iloc[row], line_width=3, legend_label=name, color=color)
#        plot.circle(x=data_summary.columns, y=data_summary.iloc[row], size=10, color=color, legend_label=name)
    plot.add_layout(Title(text=f"Scenario: {sname} baseline demand"), 'above')
    plot.add_layout(Title(text=f"Year: {year}"), 'above')
    plot.legend.label_text_font_size = '10pt'
    plot.add_layout(plot.legend[0], 'right')
    plot.legend[0].items.reverse()
#    plot.legend.location = "top_left"
    plot.legend.click_policy="hide"
    plot.xaxis.axis_label = 'Province'
    plot.yaxis.axis_label = 'Total Capacity (GW)'
    plot.yaxis.formatter = NumeralTickFormatter(format='0,0')
    plot.xgrid.visible = False
    plot.ygrid.visible = False
    plot.axis.axis_label_text_font_style = 'bold'
#    plot.legend.orientation = "horizontal"
#    show(plot)
#    output_file("plots/Stacked_bar_chart.html")
#    save(plot)
    return plot

def change_plot(Scenario, Growth, Year, Region, view_fn=import_data_summary):
    return view_fn(Scenario, Growth, Year, Region)
    

def main():
#    data_summary = import_data_summary()
    interact_dict = dict(Scenario=["Existing Transmission", "Unconstrained Transmission Expansion"], Growth=["1.5x load growth", "2.5x load growth"], Year=["2030", "2040", "2050"], Region=["All", "West"])
    i = pn.interact(change_plot, **interact_dict)
#    plot_carbon_limit_data(data_summary)
#    i.pprint()
    p = pn.Row(i[1],i[0])
    p.save('plots/ABA_generation.html', embed=True)
    p.show()
if __name__ == "__main__":
    main()
