from bokeh.models.sources import ColumnDataSource, ColumnarDataSource
from bokeh.plotting import figure, show
from bokeh.palettes import Category20 as palette
from bokeh.io import output_file, save
from bokeh.palettes import Spectral4
from bokeh.colors import RGB
from bokeh.models import Title, NumeralTickFormatter, BasicTickFormatter, PrintfTickFormatter
import openpyxl
from openpyxl import load_workbook
import pandas as pd
import panel as pn
from panel.interact import interact, interactive, fixed, interact_manual
from panel import widgets
import random
import itertools

##========= Import the data from Data_summary ============##
colour = itertools.cycle(palette)
co1, co2 = [], []
for i in range(0,50):
    r = random.randint(0,255)
    g = random.randint(0,255)
    b = random.randint(0,255)
    color = RGB(r,g,b)
    co1.append(color)
co2 = [co1[0], co1[1], co1[2], co1[3],co1[4], co1[27], co1[48], co1[49]]
#for i in range(0,8):
#    r = random.randint(0,255)
#    g = random.randint(0,255)
#    b = random.randint(0,255)
#    color = RGB(r,g,b)
#    co2.append(color)
def import_data_summary(reg, grow):
    if reg == "All":
        colours = co1
        height = 1800
        if grow == "1.5x load growth":
            sname = "Unconstrained Transmission at 1.5x"
            data_summary = pd.read_excel('Results_summary_un1.5.xlsx', sheet_name=4, index_col=0, header=0, usecols=[0]+[i for i in range(1,4)])
            data_summary = data_summary.T
        elif grow == "2.5x load growth":
            sname = "Unconstrained Transmission at 2.5x"
            data_summary = pd.read_excel('Results_summary_un2.5.xlsx', sheet_name=4, index_col=0, header=0, usecols=[0]+[i for i in range(1,4)])
            data_summary = data_summary.T
    elif reg == "West":
        colours = co2
        height = 850
        if grow == "1.5x load growth":
            sname = "Unconstrained Transmission at 1.5x"
            data_summary = pd.read_excel('Results_summary_un1.5.xlsx', sheet_name=4, index_col=0, header=0, usecols=[0]+[i for i in range(1,4)])
            data_summary.index.name = 'transmission'
            options = ['Alberta', 'British Columbia', 'Manitoba', 'Saskatchewan']
            new_data = data_summary[data_summary.index.str.contains('Manitoba|Alberta|British Columbia|Saskatchewan',)]
#            print(new_data)
            data_summary = new_data
            data_summary = data_summary.T
        elif grow == "2.5x load growth":
            sname = "Unconstrained Transmission at 2.5x"
            data_summary = pd.read_excel('Results_summary_un2.5.xlsx', sheet_name=4, index_col=0, header=0, usecols=[0]+[i for i in range(1,4)])
            data_summary.index.name = 'transmission'
            options = ['Alberta', 'British Columbia', 'Manitoba', 'Saskatchewan']
            new_data = data_summary[data_summary.index.str.contains('Manitoba|Alberta|British Columbia|Saskatchewan',)]
            data_summary = new_data
            data_summary = data_summary.T
        

    ran = 60
#    data_summary = data_summary.T
#    data_summary = data_summary.rename(index=lambda x: x[:-4], inplace=False)
    data_summary.index.name = 'Year'
#    data_summary = data_summary.groupby(['Province'], sort=False).sum()
#    data_summary = data_summary[['hydro', 'nuclear', 'coal', 'diesel', 'biomass', 'gasCC', 'gasSC', 'wind', 'solar']]
    data_summary = data_summary/1000
#    data_summary.rename(columns={"gasCC":"Natural Gas CC", "gasSC":"Natural Gas SC"})
    print(data_summary)
#    a = data_summary.index.tolist()
#    print(a)
#    return data_summary

    regions = data_summary.columns.tolist()
    source = ColumnDataSource(data_summary)
    year = source.data['Year'].tolist()
    plot = figure(title=f"Canada New Installed Transmission Capacity", x_range=year, y_range=[0,ran], plot_height=height, plot_width=800, toolbar_location=None, tools="")
    plot.vbar_stack(regions, x = 'Year', width=0.5, color=colours, source=source, legend_label=regions)
    plot.add_layout(Title(text=f"Scenario: {sname} baseline demand"), 'above')
#    plot.add_layout(Title(text=f"Year: {year}"), 'above')
    plot.legend.label_text_font_size = '10pt'
    plot.add_layout(plot.legend[0], 'below')
#    plot.legend[0].items.reverse()
#    plot.legend.location = "top_left"
    plot.legend.click_policy="hide"
    plot.xaxis.axis_label = 'Year'
    plot.yaxis.axis_label = 'New Installed Capacity (GW)'
    plot.yaxis.formatter = NumeralTickFormatter(format='0,0')
    plot.xgrid.visible = False
    plot.ygrid.visible = False
    plot.axis.axis_label_text_font_style = 'bold'
#    plot.legend.orientation = "horizontal"
#    show(plot)
#    output_file("plots/Stacked_bar_chart.html")
#    save(plot)
    return plot

def change_plot(Region, Growth, view_fn=import_data_summary):
    return view_fn(Region, Growth)
    

def main():
#    data_summary = import_data_summary()
    interact_dict = dict(Region=["All", "West"], Growth=["1.5x load growth", "2.5x load growth"])
    i = pn.interact(change_plot, **interact_dict)
#    plot_carbon_limit_data(data_summary)
#    i.pprint()
    p = pn.Row(i[1],i[0])
    p.save('plots/Transmission_capacity.html', embed=True)
    p.show()
if __name__ == "__main__":
    main()
